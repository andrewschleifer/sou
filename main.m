int NSApplicationMain(int argc, const char * _Nonnull *argv);

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
