
About
---------

This application collects information about your email inbox – specifically,
the number of unread messages – and displays it in a small notification window.

That window floats above all the desktop windows (even above the screensaver),
can be moved by dragging it around, and launches Mail.app when double-clicked.


Questions
---------

Q. Why does it only display an exclamation point?  
A. The Mail application must be running for Sou to collect the information it needs.
Double-click the notification window to launch Mail, then hide or minimize it.

Q. How do I quit?  
A. Right-clicking on the notification window will show a menu. That menu has a "Quit"
option.

Q. Does it work with Thunderbird (or other email client)?  
A. No, only Apple Mail.
